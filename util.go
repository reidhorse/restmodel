package main

import (
	"encoding/json"
	"log"
	"net/http"
	"runtime"
)

type SimpleMessage struct {
	Message string `json:"message"`
}

type ErrorMessage struct {
	Error string
}

func Err400(err error, w http.ResponseWriter) {
	log.Println(err.Error())
	_, filename, line, _ := runtime.Caller(1)
	log.Printf("[error] %s:%d %v", filename, line, err)
	JsonWithStatus(ErrorMessage{err.Error()}, 400, w)
}

func Say200(w http.ResponseWriter) {
	JsonWithStatus(SimpleMessage{"ok"}, 200, w)
}

func JsonObj200(o interface{}, w http.ResponseWriter) {
	JsonWithStatus(o, 200, w)
}

func Json200(message string, w http.ResponseWriter) {
	JsonObj200(SimpleMessage{message}, w)
}

func JsonWithStatus(o interface{}, status int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(o)
}
