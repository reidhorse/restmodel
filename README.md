# REST Model
The purpose of this code is to demonstrate the generation and marriage of a REST endpoint and a database model using the Go programming language.

# Overview
 

# Setup

## 1. docker-compose
A `docker-compose.yml` file has been created for you to set up the necessary components.
They are:

* Postgres Database 
* Swagger Editor Web UI
* Adminer Web UI

Start everything using: 
    
    docker-compose up -d

## 2. SQLBoiler
This project makes use of [SQLBoiler](https://github.com/volatiletech/sqlboiler) to generate a Go model from an existing database schema.

### a) Installation
Please read the [Getting Started](https://github.com/volatiletech/sqlboiler#getting-started) docs for more details on SQLBoile installation. In short, do:

    go install github.com/volatiletech/sqlboiler/v4@latest
    go install github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql@latest

### b) Generation
This command will read the schema in Postgres and generate Go model code in `./models`

    sqlboiler psql


## 3. Swagger (optional)
The REST endpoint code for this project was generated using the Swagger Editor accessible at [http://localhost:8082](http://localhost:8082).

You must copy/paste the contents of `swagger.yaml` into the browser's editor to view the API spec.

### Generation
To generate the REST API code, simply click `Generate Server` -> `Go Server` and unpack the downloaded archive.

This work has already been done for you.

## 4. Build
Initialize the project's dependencies:

    go mod tidy
   
Build the project:

    go build -o restmodel
   
## 5. Run
Run the REST endpoint with:
    
    ./restmodel


## 6. Play
You can spin up [Postman](https://www.postman.com/) or [Insomnia](https://insomnia.rest/) to play around with the API. With either tool, you can import the `swagger.yaml` document to get started quickly.