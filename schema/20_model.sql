\c restmodel

create table if not exists profile
(
    id    serial primary key,
    email varchar not null unique,
    name  varchar not null
);

create table if not exists movie
(
    id   serial primary key,
    name varchar not null
);

create table if not exists favorite
(
    profile_id serial references profile (id),
    favorite_id   serial references movie (id),
    primary key (profile_id, favorite_id)
);

create table if not exists actor
(
    id   serial primary key,
    name varchar not null,
    lastname varchar not null
);

create table if not exists actor_movie
(
    actors_id serial references actor (id),
    movies_id serial references movie (id),
    primary key (actors_id, movies_id)
);

