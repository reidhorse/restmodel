create database restmodel
    with owner postgres
    encoding 'UTF8'
    tablespace pg_default;